int pId =0; //declaração do numero do processo que vai identificar o programa a ser aberto. Como só será aberto um programa, então o primeiro processo é o zero.
           //esse valor porderia ser passado diretamente na strimg como um char, sem a necessidade de ser declarado como int.

//criação de um vetor de caracteres que representam cada uma das teclas que poderão ser precionadas no programa            
char caractere[37]={'1','2','3','4','5','6','7','8','9','0','q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','ç','z','x','c','v','b','n','m'};

int estado=0;  //"estado" é a variável definida como status para informar se há sinal de áudio ou não.

int leitura_audio; //como o próprio nome já diz, é a leituira do sinal de áudio na porta analógica

int zeros=0; //variável definida para decidir se há sinal de áudio ou não
char atual; //tecla atual a ser pressionada no arkaos
char anterior='|'; //telca qualquer sem função para quando não houver áudio. é uma tecla sem função
int i; //indice do vetor de caracteres


void leitura(char comand_atual)
{
  if (leitura_audio > 0) //Se o valor lido na porta analógica (A0) for maior que zero, então serão executadas as instruções dentro deste if
  {   
      aciona_tecla(comand_atual);
      delay(1500);
      aciona_tecla(anterior);
      delay(3000);
      anterior=atual;
      zeros=0;
      estado=1;
  }
 
  else if (leitura_audio==0)
  { 
      ++zeros;
                      
            if(zeros>3 & estado==1)//caso o estado seja igual a 1 significa que a ultima tecla pressionada precisar ser desativada e então após issoo nenhuma tecla estará ativa 
                        //e o estado pode ser definido como 0, que significa que não há sinal de áudio
            {
            aciona_tecla(anterior); //aciona tecla anterior (última tecla acionada) para parar a execução dela, pois não há sinal de áudio
            estado=0; //definide o estado como sendo 0, desativado, ou seja, não há sinal de áudio
            zeros=0; //esta variável volta ao estado inicial 0 para que o programa volte a analizar se há sinal de áudio ou não, por que se ela não for zerada, um unico valor 0 
                    //na leitura poderia fazer o programa entender que não há sinal de áudio já que o numero de zeros continuaria sendo maior que 0 se eu não voltar a 0 a variável
                   //e um único 0 na leitura não é suficiente para dizer que não há sinal de áudio, pois o mesmo em sua natureza pode apresentar alguns poucos zeros em distintos intervalos
            anterior='|'; //a tecla anterior é definida como uma tecla qualquer, sem função, pois não há nenhuma tecla anterior ativa ou existente
            }
          
  delay(200);
  }          
          

}


//função que imprime as strimgs de comando para o programa e para abarí-lo
void aciona_tecla(char tecla)
{   
   Serial.print("#S|SENDK|[");
   Serial.print(pId);//poderia ser escrito diretamente como um char, sem a necessidade de ser declarado como int
   Serial.print("&");
   Serial.print(tecla);
   Serial.println("]#"); 
  
}

void setup() 
{ 
  Serial.begin(9600);//configuração padrão do Arduino. Define a taxa de dados em bits por segundo (baud) para a transmissão serial.
  
  
  Serial.println("#S|ABRIR|[]#");  
  delay(9000);//9 segundos de espera até que o programa seja aberto
   Serial.print("#S|SENDK|[");
   Serial.print(pId);
   Serial.print("&");
   Serial.print("^");
   Serial.print("a");
   Serial.println("]#"); 
   delay(9000);
} 
 
void loop() 
{ 

  leitura_audio=analogRead(0);
  i=random()%37; //seleciona um numero aleatório de 1 a 37
  atual=caractere[i];// essa variável representa a tecla atual a ser pressionada
  leitura(atual);//chama a função "leitura" e passa o valor da variável "atual" 
 

}
   
   

  




