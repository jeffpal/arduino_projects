#include <AFMotor.h>
#include <IRremote.h>
#include <IRremoteInt.h>

int RECV_PIN = 2;
IRrecv irrecv(RECV_PIN);
AF_DCMotor motor(3, MOTOR12_1KHZ);
AF_DCMotor motor2(4, MOTOR12_1KHZ);
decode_results results;

void setup(){
   Serial.begin(9600);   
   motor.setSpeed(255);
   motor2.setSpeed(255);
   irrecv.enableIRIn();
}

void loop(){
    if (irrecv.decode(&results)){
      //frente  
        if(results.value==650130){
            motor.run(FORWARD);
            motor2.run(BACKWARD);
        }
        //tras
        if(results.value==387986){
            motor.run(BACKWARD);
            motor2.run(FORWARD);            
        }        

        //direita
        if(results.value==256914){
            //motor.run(BACKWARD);
            //motor2.run(BACKWARD);    
            motor2.run(BACKWARD);         
        }     
        //esquerda
        if(results.value==912274){

            //motor.run(FORWARD);
            //motor2.run(FORWARD);
            motor.run(FORWARD);            
        }
         //Serial.println(results.value);
         irrecv.resume();   
         
    }
    delay(250);
    motor.run(RELEASE);
    motor2.run(RELEASE);
    
}
