#include <IRremote.h>
int RECV_PIN = 11;
IRrecv irrecv(RECV_PIN);
int tecla;
decode_results results;
char buffer[5];
char serInString[25];
int pId =0;

void setup()
{
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
  pId= atoi(serInString);
  Serial.println("#S|EXECUTAR|[]#");
}

void loop() {
  if (irrecv.decode(&results)) {
    tecla=results.value;
    //Serial.println(tecla, DEC);
        
    if (tecla==-14281){
     Serial.print("#S|SENDK|[");
     Serial.print(itoa((pId), buffer, 10));
     Serial.print("&");  
     Serial.print("{F11}");
     Serial.println("]#"); 
   
     delay(500); 
    }    
  
    else if (tecla==12495){
  
     Serial.print("#S|SENDK|[");
     Serial.print(itoa((pId), buffer, 10));
     Serial.print("&");  
     Serial.print("{ESC}");
     Serial.println("]#"); 
   
     delay(500); 
    }    
      
    else if (tecla==24735){
  
     Serial.print("#S|SENDK|[");
     Serial.print(itoa((pId), buffer, 10));
     Serial.print("&");  
     Serial.print("{RIGHT}");
     Serial.println("]#"); 
   
     delay(500); 
    }
    
    else if (tecla==-8161){
    
     Serial.print("#S|SENDK|[");
     Serial.print(itoa((pId), buffer, 10));
     Serial.print("&");  
     Serial.print("{LEFT}");
     Serial.println("]#"); 
   
     delay(500); 
    }
    
    irrecv.resume(); // Receive the next value
  }
}
