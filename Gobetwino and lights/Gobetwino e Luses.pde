/* 

            DESCRIÇÃO DO CÓGIDO
            
    Este código tem a função de executar algumas funções do computador
    através de um controle IR-TV, com o Gobetwino, e controlar alguns pinos de saída
    para ligar e desligar algum dispositivo externo.

*/



#include <IRremote.h>
int RECV_PIN=11;
IRrecv irrecv (RECV_PIN);
decode_results results;
char serInString[25];
int pId =0;
int estado=0;

void teste(int pin)
{
  estado++;
  
  if(estado==2)
  {
  digitalWrite(pin,HIGH);
  delay(250);
  }
  
  else if(estado==4)
  {
  digitalWrite(pin,LOW);
  estado=0;
  delay(250);
  }
} 

void setup() 
{ 
  Serial.begin(9600); 
  irrecv.enableIRIn();
  pId= atoi(serInString);               
  pinMode(13,OUTPUT);                       
  pinMode(12,OUTPUT);                       
} 
 
void loop() 
{ 

  if (irrecv.decode(&results))
  {
   Serial.println(results.value);
   irrecv.resume(); 
   delay(50);
   
   if(results.value==2962) 
   teste(13);
   
   if(results.value==527250) 
   teste(12);
   
   if(results.value==314258) 
   {
   Serial.println("#S|EXECUTAR01|[]#");   
   delay(1000);
   }   
  
  
  if(results.value==789394) 
   {
   Serial.println("#S|EXECUTAR02|[]#");   
   delay(1000);
   }   
   
     if(results.value==134034) 
   {
   Serial.println("#S|EXECUTAR03|[]#");   
   delay(1000);
   } 
   
     if(results.value==658322) 
   {
   Serial.println("#S|EXECUTAR04|[]#");   
   delay(1000);
   } 
   
     if(results.value==396178) 
   {
   Serial.println("#S|EXECUTAR05|[]#");   
   delay(1000);
   } 
   
   //sair 
   if(results.value==2704) 
    {
   char buffer[5];
   
   Serial.print("#S|SENDK|[");
   Serial.print(itoa((pId), buffer, 10));
   Serial.print("& ");
   Serial.print("%{F4}");
   Serial.println("]#"); 
   
   delay(2000);   
   }


   if(results.value==691090) 
    {
   char buffer[5];
   
   Serial.print("#S|SENDK|[");
   Serial.print(itoa((pId), buffer, 10));
   Serial.print("& ");
   Serial.print("^q");
   Serial.println("]#"); 
   
   delay(2000);   
   }

  if(results.value==2640) 
    {
   char buffer[5];
   
   Serial.print("#S|SENDK|[");
   Serial.print(itoa((pId), buffer, 10));
   Serial.print("& ");
   Serial.print("{F5}");
   Serial.println("]#"); 
   
   delay(2000);   
   }
   
   

  }  
  
} 


