#include <Keypad.h>

const byte ROWS = 4; //four rows
const byte COLS = 3; //four columns
//define the cymbols on the buttons of the keypads
char teclado[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};
byte rowPins[ROWS] = {13, 12, 11, 10}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {9, 8, 7}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(teclado), rowPins, colPins, ROWS, COLS);

void setup(){
  Serial.begin(57600);
  pinMode(6,OUTPUT);
  digitalWrite(6,HIGH);
}
 int conta=0;
void loop(){
  int customKey = customKeypad.getKey();
  
  if (customKey){
    Serial.println(customKey);
    conta=conta+customKey;
    Serial.println(conta);
    
    if(conta==150){
      digitalWrite(6,LOW);
      conta=0;
      Serial.println("SENHA CORRETA PARA LIGAR");
//      delay(300);
//      digitalWrite(6,HIGH);
    }

    if(conta==159){
      digitalWrite(6,HIGH);
      conta=0;
      Serial.println("SENHA CORRETA PARA DESLIGAR");
    }
    
    if(conta==42){
      conta=0;
      Serial.println("REINICIADO");
    }
    
    else if (conta>125)
    {
      Serial.println("SENHA INCORRETA");
      conta=0;
    }
  }
}
